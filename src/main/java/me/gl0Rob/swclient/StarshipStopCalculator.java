/**
 * 
 */
package me.gl0Rob.swclient;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import me.gl0Rob.swclient.api.StarshipApi;
import me.gl0Rob.swclient.exception.StarWarsException;
import me.gl0Rob.swclient.model.StarshipModel;

/**
 * @author robertgloster
 *
 */
public class StarshipStopCalculator {
	
	@Autowired
	private StarshipApi api;
	
	private static final Logger log = LoggerFactory.getLogger(StarshipStopCalculator.class);
	
	/**
	 * Method to calculate the number of stops for each starship 
	 * @param distance
	 * @return String A list of starships and the number of stops required
	 * @throws StarWarsException
	 */
	public String caculateStopsForAllStarships(Float distance) throws StarWarsException{
		
		log.info("Calculating stops for all starships");
		List<StarshipModel> starships = api.getAll();
		
		StringBuilder sb = new StringBuilder();
		int consumableTimeInHours;
		int speedInMGLT;
		int numberOfStops;
		
		for(StarshipModel ship : starships){
			log.debug("Calculating stops for : " + ship.getName());
			consumableTimeInHours = CalculatorUtils.parseUnits(ship.getConsumables());
			speedInMGLT = CalculatorUtils.parseInteger(ship.getMGLT());
			
			sb.append(ship.getName());
			sb.append(": ");
		
			log.debug("Formula inputs - speedInMGLT: " + speedInMGLT + " - consumableTimeInHours: " + consumableTimeInHours + " - distance: " + distance);
			if(speedInMGLT != 0 && consumableTimeInHours != 0){
				numberOfStops = (int) Math.floor(distance/(consumableTimeInHours * speedInMGLT));
				sb.append(numberOfStops);
				log.debug("Number of stop :" + numberOfStops);
			} else {
				log.debug("Number of stop : NA");
				sb.append("NA");
			}
	
			sb.append(System.lineSeparator());
			
			
		}
		
		return sb.toString();
		
	}
	

}
