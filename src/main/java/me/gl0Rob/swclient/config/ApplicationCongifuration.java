package me.gl0Rob.swclient.config;

import java.util.Arrays;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.web.client.RestTemplate;

import me.gl0Rob.swclient.StarshipStopCalculator;
import me.gl0Rob.swclient.api.StarshipApi;

/**
 * Application configuration
 * Specify Beans to be injected into the application
 * @author robertgloster
 *
 */
@Configuration
public class ApplicationCongifuration {
	
	@Autowired
	private RestTemplate restTemplate;
	
	@Autowired
	private HttpEntity<String> entity;
	
	
	@Bean
	public RestTemplate getRestTemplate(){
		return new RestTemplate();
	}
	
	@Bean
	public HttpEntity<String> getHttpEntity(){
		
		HttpHeaders headers = new HttpHeaders();
	    headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
	    headers.set("User-Agent", "swapi-Java-rob");
	    HttpEntity<String> entity = new HttpEntity<String>("parameters", headers);
	    return entity;
		
	}
	
	@Bean
	public StarshipApi getStarshipApi(){
		
		return new StarshipApi(restTemplate, entity);
		
	}
	
	@Bean
	public StarshipStopCalculator getStarshipStopCalculator(){
		return new StarshipStopCalculator();
	}
	
}
