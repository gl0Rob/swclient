package me.gl0Rob.swclient;

public class CalculatorUtils {
	
	private static final int NUMBER_OF_DAYS_PER_YEAR = 365;
	private static final int NUMBER_OF_DAYS_PER_WEEK = 7;
	
	//Prevent instantiation 
	private CalculatorUtils(){
		
	}
	
	
	/**
	 * Function to the consumable data returned by swapi. 
	 * @param input example "1 week"
	 * @return Integer the number of hours of consumables available
	 */
	public static Integer parseUnits(String input){
		
		if( input != null && !input.isEmpty()){
			String[] data = input.split(" ");
			String unit;
			Integer amount;
			if(data.length == 2){
				amount = parseInteger(data[0]);
				
				unit = data[1].toUpperCase();
				//Remove 's' form the end of time units 
				if("s".equalsIgnoreCase(unit.substring(unit.length() - 1))){
					unit = unit.substring(0, unit.length() - 1);
				}
				
				switch(unit.toUpperCase()){
				case "HOUR" : 
					return amount;
				case "DAY" :
					return amount * 24;
				case "WEEK" :
					return amount * 24 * NUMBER_OF_DAYS_PER_WEEK;
				case "MONTH" :
					return amount * (24 * NUMBER_OF_DAYS_PER_YEAR)/12 ;
				case "YEAR" :
					return amount * 24 * NUMBER_OF_DAYS_PER_YEAR;
				default :
					return 0;
					
				}
				
			} else {
				//Report format error
			}
			
			
		}
		
		return 0;
		
	}
	
	/**
	 * @param unit 
	 * @return Integer
	 */
	public static Integer parseInteger(String unit){
		
		try {
			return Integer.parseInt(unit);
		}
		catch(NumberFormatException nfe){
			return 0;
		}
	}

}
