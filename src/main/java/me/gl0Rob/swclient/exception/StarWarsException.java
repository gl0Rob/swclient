/**
 * 
 */
package me.gl0Rob.swclient.exception;

import org.springframework.web.client.ResourceAccessException;

/**
 * Custom StarWarsException exception to catch checked exceptions and provide additional info
 * @author robertgloster
 *
 */
public class StarWarsException extends Exception{

	/**
	 * 
	 */
	private static final long serialVersionUID = -8662046696335804847L;
	
	String info;
	
	public StarWarsException(Exception ex) {
		super(ex);
		this.info = "No additional Info";
	}
	
	public StarWarsException(Exception ex, String info) {
		super(ex);
		this.info = info;
	}


	public String getInfo() {
		return info;
	}

}
