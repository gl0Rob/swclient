/**
 * 
 */
package me.gl0Rob.swclient.api;

import org.springframework.http.HttpEntity;
import org.springframework.web.client.RestTemplate;

import me.gl0Rob.swclient.model.StarshipModel;

/**
 * Starship API - Us this class to retrieve starship data from swapi.co
 * @author robertgloster
 *
 */
public class StarshipApi extends AbstractStarWarsApi<StarshipModel> {
	
	//Entry point for the starship api url
	public static final String STARSHIP_URL = "/starships/";
	
	/**
	 * @param template
	 * @param entity
	 */
	public StarshipApi(RestTemplate template, HttpEntity<String> entity) {
		super(template, entity, STARSHIP_URL);
	}	
}
