/**
 * ParameterizedTypeReference is used only to provide Type to the template object mapper when 
 * genierics are used. Overriding its getType() method to provide support for generics nested one level lower.
 * 
 * See https://stackoverflow.com/questions/21987295/using-spring-resttemplate-in-generic-method-with-generic-parameter/29547365#29547365 for additional info regarding the work around
 */
package me.gl0Rob.swclient.api;

import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;

import org.springframework.core.ParameterizedTypeReference;

import me.gl0Rob.swclient.model.StarshipModel;
import me.gl0Rob.swclient.model.StarwarsListResponse;

/**
 * @author robertgloster
 *
 */
public class StarwarsParameterizedTypeReference<T> extends ParameterizedTypeReference<T> {

	
	@Override
	public Type getType() {
		Type [] responseWrapperActualTypes = {StarshipModel.class};
		ParameterizedType responseWrapperType = new ParameterizedTypeImpl(StarwarsListResponse.class,responseWrapperActualTypes,null);
		return responseWrapperType;
	}
}
