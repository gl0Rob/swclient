/**
 * 
 */
package me.gl0Rob.swclient.api;

import java.util.List;

import me.gl0Rob.swclient.exception.StarWarsException;

/**
 * @author robertgloster
 *
 */
public interface StarWarsApi<T> {
	
	public static final String API_BASE_URL = "https://swapi.co/api/";
	
	
	/**
	 * Get All - This method will concatenate all pages of returned results provided by the swapi 
	 * @return <code>null</code> if no results are returned
	 * @throws StarWarsException 
	 */
	public List<T> getAll() throws StarWarsException;
	
	/**
	 * Get All
	 * @param page Page number. 
	 * @return <code>null</code> if no starships are returned
	 * @throws StarWarsException 
	 */
	public List<T> getAll(Integer page) throws StarWarsException;
	
	/**
	 * @param id The unique identifer of the ship to be returned
	 * @return <code>null</code> if not ships is found;
	 * @throws StarWarsException 
	 */
	public T get(Integer id) throws StarWarsException;

}
