/**
 * 
 */
package me.gl0Rob.swclient.api;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.util.Assert;
import org.springframework.web.client.ResourceAccessException;
import org.springframework.web.client.RestTemplate;

import me.gl0Rob.swclient.exception.StarWarsException;
import me.gl0Rob.swclient.model.StarwarsListResponse;

/**
 * AbstractStarWarsApi that provides default implementation of the StarWarsApi
 * This class should be extended by each API entry point
 * @author robertgloster
 * @param <T> Model class
 *
 */
public abstract class AbstractStarWarsApi<T> implements StarWarsApi<T> {
	
	private static final Logger log = LoggerFactory.getLogger(AbstractStarWarsApi.class);
	
	private RestTemplate template;
	
	private HttpEntity<String> entity;
	
	private String entry;
	
	
	/**
	 * @param template
	 * @param entity
	 * @param entry
	 */
	public AbstractStarWarsApi(RestTemplate template, HttpEntity<String> entity, String entry){
		log.debug("API url: " + API_BASE_URL + entry);
		
		Assert.notNull(template, "Template must not be null");
		Assert.notNull(entity, "Entity must not be null");
		Assert.notNull(entry, "Entry url must not be null");
		this.template = template;
		this.entity = entity;
		this.entry = entry;
	}
	
	/**
	 * Get All - This method will concatenate all pages of returned results provided by the swapi 
	 * @return <code>null</code> if no results are returned
	 * @throws StarWarsException 
	 */
	public List<T> getAll() throws StarWarsException{
		List<T> result = new ArrayList<T>();
		int page = 1;
		ResponseEntity<StarwarsListResponse<T>> response = requestPage(page);
		result.addAll(response.getBody().getResults());
		while(response.getBody().getNext() != null){
			page++;
			response = requestPage(page);
			result.addAll(response.getBody().getResults());
		}
		return result;
	}
	
	/**
	 * Get All
	 * @param page Page number. 
	 * @return <code>null</code> if no starships are returned
	 * @throws StarWarsException 
	 */
	public List<T> getAll(Integer page) throws StarWarsException{
		return requestPage(page).getBody().getResults();
	}
	
	
	/**
	 * private method for requesting single page
	 * @param page
	 * @return
	 * @throws StarWarsException
	 */
	private ResponseEntity<StarwarsListResponse<T>> requestPage(Integer page) throws StarWarsException{
		try{
			return template.exchange(StarWarsApi.API_BASE_URL + entry + "?page=" + page, HttpMethod.GET, entity, new StarwarsParameterizedTypeReference<StarwarsListResponse<T>>(){});
		} catch (ResourceAccessException ex){
			log.warn(ex.toString());
			throw new StarWarsException(ex, "Unable to reach swapi, please try again later");
		}
	
	}
	
	/**
	 * @param id The unique identifer of the ship to be returned
	 * @param clazz The expected response type
	 * @return <code>null</code> if not ships is found;
	 * @throws StarWarsException 
	 */
	public T get(Integer id) throws StarWarsException{
		try{
			ResponseEntity<T> result = template.exchange(StarWarsApi.API_BASE_URL + entry + id, HttpMethod.GET, entity, new ParameterizedTypeReference<T>(){});
			return result.getBody();
		} catch (ResourceAccessException ex){
			log.warn(ex.toString());
			throw new StarWarsException(ex, "Unable to reach swapi");
		}
	}
	

}
