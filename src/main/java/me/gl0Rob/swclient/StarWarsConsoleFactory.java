package me.gl0Rob.swclient;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;

/**
 * Factory to generate BufferedReader & BufferedWriter
 * @author robertgloster
 *
 */
public class StarWarsConsoleFactory {

	public static BufferedReader createReader() {
		return new BufferedReader(new InputStreamReader(System.in));		
	}

	public static BufferedWriter createWriter() {
		return new BufferedWriter(new OutputStreamWriter(System.out));
		
	}

}
