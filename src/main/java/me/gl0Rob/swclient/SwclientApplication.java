package me.gl0Rob.swclient;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;

import org.apache.tomcat.util.buf.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.util.Assert;
import org.springframework.util.NumberUtils;

import me.gl0Rob.swclient.exception.StarWarsException;

/**
 * Spring boot SW Client. 
 * Used to connect to the swapi and determine the number of stops each starship will be required to make for a given distance
 * @author robertgloster
 *
 */
@SpringBootApplication
public class SwclientApplication implements CommandLineRunner{

	private static final Logger log = LoggerFactory.getLogger(SwclientApplication.class);

	@Autowired
	private SwclientApplication instance;
	
	@Autowired
	private StarshipStopCalculator calc;
	
	public static void main(String[] args) throws IOException {
		SwclientApplication.createInstance();
	}
	
	@Override
	public void run(String... args) throws Exception {	
		System.out.print("A long time ago in a galaxy far far away...." + System.lineSeparator());
		Assert.notNull(this.getInstance(), "Application must be instantiated first");
		this.getInstance().promptInput();
		
	}


	protected SwclientApplication getInstance() {
		return instance;
	}

	
	/**
	 * Prompt user for distance input and display number of stops required for each starship 
	 * @throws IOException
	 */
	private void promptInput() throws IOException {
		
		Boolean running = true;
		log.debug("Prompting for user input");
		
		StarWarsConsoleFactory.createWriter();
		BufferedReader in = StarWarsConsoleFactory.createReader();
		BufferedWriter out = StarWarsConsoleFactory.createWriter();
		String input = "";
		Float distance;
		while(running){
			
			out.write("Enter Distence in MGLT:" + System.lineSeparator());
			out.flush();
			input = in.readLine().toLowerCase();
			
			log.debug("Input from console :",input);
			
			switch (input){
				case "q": 
				case "quit":
				case "exit": 
					log.debug("Exiting application");
					out.write(System.lineSeparator() + "May the force be with you!" + System.lineSeparator());
					out.flush();
					out.close();
					in.close();
					running = false;
					break;
				default:
					try {
						//Possibly a little to forgiving in the number format for example 100d is perfecty acceptable
						distance = Float.parseFloat(input);
						out.write("Calculating Stops . . . ." + System.lineSeparator());
						out.flush();
						try { 
							out.write(caculateStops(distance) + System.lineSeparator());
						} catch (StarWarsException ex) {
							out.write("It's a Trap!  - Ooops somethings gome wrong! " + ex.getInfo() + System.lineSeparator());
						}
					}
					catch(NumberFormatException nfe){
						log.debug(nfe.toString());
						out.write("This is not the format we are looking for! - Numeric input expected - To exit enter 'exit'" + System.lineSeparator());
					}
					break;
					
			}
			out.flush();
		} 					
		
		System.exit(0);


	}
	
	
	/**
	 * Caculate the number of stops for all Starships
	 * @param distance
	 * @return
	 * @throws StarWarsException
	 */
	private String caculateStops(Float distance) throws StarWarsException{
		return calc.caculateStopsForAllStarships(distance);
	}

	

	/**
	 * Start Application
	 */
	private static void createInstance(){
		SpringApplication app = new SpringApplication(SwclientApplication.class);
		app.run();
	}

}
