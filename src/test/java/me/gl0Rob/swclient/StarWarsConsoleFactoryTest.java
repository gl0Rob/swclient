package me.gl0Rob.swclient;

import org.junit.Test;

import static org.junit.Assert.*;

import java.io.BufferedReader;
import java.io.BufferedWriter;

public class StarWarsConsoleFactoryTest {
	
	@Test
	public void createReaderTest(){
		BufferedReader reader = StarWarsConsoleFactory.createReader();
		assertNotNull(reader);
		
		BufferedReader reader2 = StarWarsConsoleFactory.createReader();
		assertNotSame(reader, reader2);
		
	}
	
	@Test
	public void createWriterTest(){
		 BufferedWriter writer = StarWarsConsoleFactory.createWriter();
		assertNotNull(writer);
		
		 BufferedWriter writer2 = StarWarsConsoleFactory.createWriter();
		assertNotSame(writer, writer2);
		
	}

}
