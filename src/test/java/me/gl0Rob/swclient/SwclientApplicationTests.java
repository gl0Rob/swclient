package me.gl0Rob.swclient;


import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.*;

import java.io.BufferedReader;
import java.io.BufferedWriter;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import org.powermock.reflect.Whitebox;


//PowerMock will cause anomaly in code coverage 
@RunWith(PowerMockRunner.class)
@PrepareForTest({StarWarsConsoleFactory.class,SwclientApplication.class, System.class})
public class SwclientApplicationTests {
		
	@Test
	public void promptOnceThenQuitTest() throws Exception {
		
		BufferedReader br = mock(BufferedReader.class);
		BufferedWriter bw = mock(BufferedWriter.class);
		PowerMockito.mockStatic(StarWarsConsoleFactory.class);
		PowerMockito.mockStatic(System.class);
		PowerMockito.when(StarWarsConsoleFactory.createReader()).thenReturn(br);
		PowerMockito.when(StarWarsConsoleFactory.createWriter()).thenReturn(bw);
		
		StarshipStopCalculator mockCalc = mock(StarshipStopCalculator.class);
		
		when(br.readLine()).thenReturn("100000").thenReturn("q");
		doNothing().when(bw).write(anyString());
		
		SwclientApplication appSpy = PowerMockito.spy(new SwclientApplication());
		PowerMockito.doReturn(appSpy).when(appSpy, "getInstance");
		
		Whitebox.setInternalState(appSpy, "calc", mockCalc );
		when(mockCalc.caculateStopsForAllStarships(100000F)).thenReturn("");
		appSpy.run(null);
		
		verify(mockCalc, atMost(1)).caculateStopsForAllStarships(anyFloat());
	}
	
	@Test
	public void promptTwiceThenQuitTest() throws Exception {
		
		BufferedReader br = mock(BufferedReader.class);
		BufferedWriter bw = mock(BufferedWriter.class);
		PowerMockito.mockStatic(StarWarsConsoleFactory.class);
		PowerMockito.mockStatic(System.class);
		PowerMockito.when(StarWarsConsoleFactory.createReader()).thenReturn(br);
		PowerMockito.when(StarWarsConsoleFactory.createWriter()).thenReturn(bw);
		
		StarshipStopCalculator mockCalc = mock(StarshipStopCalculator.class);
		
		when(br.readLine()).thenReturn("100000").thenReturn("100000").thenReturn("quit");
		doNothing().when(bw).write(anyString());
		
		SwclientApplication appSpy = PowerMockito.spy(new SwclientApplication());
		PowerMockito.doReturn(appSpy).when(appSpy, "getInstance");
		
		Whitebox.setInternalState(appSpy, "calc", mockCalc );
		when(mockCalc.caculateStopsForAllStarships(100000F)).thenReturn("");
		appSpy.run(null);
		
		verify(mockCalc, atMost(2)).caculateStopsForAllStarships(anyFloat());
	}

}
