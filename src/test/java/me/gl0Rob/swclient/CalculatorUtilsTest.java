package me.gl0Rob.swclient;

import static org.junit.Assert.*;

import org.junit.Test;

public class CalculatorUtilsTest {
	
	@Test
	public void parseHourTest(){
		
		int expected = 1;
		int result = CalculatorUtils.parseUnits("1 HOUR");
		assertEquals(expected, result);
		
		expected = expected *  2;
		result = CalculatorUtils.parseUnits("2 hours");
		assertEquals(expected, result);
				
	}
	
	@Test
	public void parseWeekTest(){
		
		int expected = 24 * 7;
		int result = CalculatorUtils.parseUnits("1 WEEK");
		assertEquals(expected, result);
		
		expected = expected *  2;
		result = CalculatorUtils.parseUnits("2 Weeks");
		assertEquals(expected, result);
				
	}

	
	@Test
	public void parseDayTest(){
		
		int expected = 24;
		int result = CalculatorUtils.parseUnits("1 day");
		assertEquals(expected, result);
		
		expected = 24 * 2;
		result = CalculatorUtils.parseUnits("2 Days");
		assertEquals(expected, result);
				
	}
	
	@Test
	public void parseMonthTest(){
		
		int expected = (24 * 365)/12 ;
		int result = CalculatorUtils.parseUnits("1 Month");
		assertEquals(expected, result);
		
		expected = expected *  2;
		result = CalculatorUtils.parseUnits("2 Months");
		assertEquals(expected, result);
				
	}
	
	
	
	@Test
	public void parseYearTest(){
		
		int expected = 24 * 365;
		int result = CalculatorUtils.parseUnits("1 YEAR");
		assertEquals(expected, result);
		
		expected = expected *  2;
		result = CalculatorUtils.parseUnits("2 YEaRS");
		assertEquals(expected, result);
				
	}
	
	@Test
	public void parseInvalidData(){
		
		int expected = 0;
		int result = CalculatorUtils.parseUnits("1");
		assertEquals(expected, result);
		
		expected = 0;
		result = CalculatorUtils.parseUnits(null);
		assertEquals(expected, result);
		
		expected = 0;
		result = CalculatorUtils.parseUnits("");
		assertEquals(expected, result);
				
		expected = 0;
		result = CalculatorUtils.parseUnits("0 Parsec");
		assertEquals(expected, result);
	}
	
	@Test
	public void parseIntegerTest(){
		int expected = 1;
		int result = CalculatorUtils.parseInteger("1");
		assertEquals(expected, result);
		
		expected = 0;
		result = CalculatorUtils.parseInteger("1d");
		assertEquals(expected, result);
	}
	
	

}
