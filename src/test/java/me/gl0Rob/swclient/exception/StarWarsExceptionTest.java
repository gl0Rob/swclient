package me.gl0Rob.swclient.exception;

import static org.junit.Assert.*;

import org.junit.Test;

public class StarWarsExceptionTest {
	
	@Test
	public void getInfoTest(){
		String expected = "expected";
		Exception e = new Exception("Something");
		StarWarsException ex = new StarWarsException(e,"expected");
		
		assertEquals(expected, ex.getInfo());
		
		//Check the original exception didnt go missing;
		assertEquals("java.lang.Exception: Something", ex.getMessage());
		
	}
	
	@Test
	public void noInfoTest(){
		String expected = "No additional Info";
		Exception e = new Exception("Something");
		StarWarsException ex = new StarWarsException(e);
		
		assertEquals(expected, ex.getInfo());
		
		//Check the original exception didnt go missing;
		assertEquals("java.lang.Exception: Something", ex.getMessage());
		
	}

}
