package me.gl0Rob.swclient;

import static org.junit.Assert.*;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import me.gl0Rob.swclient.api.StarshipApi;
import me.gl0Rob.swclient.exception.StarWarsException;
import me.gl0Rob.swclient.model.StarshipModel;

@RunWith(MockitoJUnitRunner.class)
public class StarshipStopCalculatorTest {
	
	@Mock
	private StarshipApi api;
	
	@InjectMocks
	private StarshipStopCalculator calc;
	
	List<StarshipModel> starships;
	
	@Before
	public void setUp(){

		starships = new ArrayList<>();
		
	}
	
	@Test
	public void caculateStopsForAllStarshipsTest() throws StarWarsException{
		
		StarshipModel ywing = new StarshipModel();
		ywing.setConsumables("1 week");
		ywing.setName("Y-wing");
		ywing.setMGLT("80");
		
		starships.add(ywing);
		
		Float distance = 1000000F;
		String expected = "Y-wing: 74" + System.lineSeparator();
		
		when(api.getAll()).thenReturn(starships);
		String result = calc.caculateStopsForAllStarships(distance);
		assertEquals(expected, result);
		
	}
	
	@Test
	public void caculateStopsForAllStarshipsNoSpeedTest() throws StarWarsException{
		
		StarshipModel ywing = new StarshipModel();
		ywing.setConsumables("1 week");
		ywing.setName("Y-wing");
		ywing.setMGLT(null);
		
		starships.add(ywing);
		
		Float distance = 1000000F;
		String expected = "Y-wing: NA" + System.lineSeparator();
		
		when(api.getAll()).thenReturn(starships);
		String result = calc.caculateStopsForAllStarships(distance);
		assertEquals(expected, result);
		
	}
	
	@Test
	public void caculateStopsForAllStarshipsNoFoodTest() throws StarWarsException{
		
		StarshipModel ywing = new StarshipModel();
		ywing.setConsumables(null);
		ywing.setName("Y-wing");
		ywing.setMGLT("80");
		
		starships.add(ywing);
		
		Float distance = 1000000F;
		String expected = "Y-wing: NA" + System.lineSeparator();
		
		when(api.getAll()).thenReturn(starships);
		String result = calc.caculateStopsForAllStarships(distance);
		assertEquals(expected, result);
		
	}

}
