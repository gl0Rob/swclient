package me.gl0Rob.swclient.api;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.ResourceAccessException;
import org.springframework.web.client.RestTemplate;

import me.gl0Rob.swclient.exception.StarWarsException;
import me.gl0Rob.swclient.model.StarshipModel;
import me.gl0Rob.swclient.model.StarwarsListResponse;

import static org.junit.Assert.*;
import static org.mockito.Mockito.*;

import java.util.ArrayList;
import java.util.List;

@RunWith(MockitoJUnitRunner.class)
public class StarshipApiTest {
	
	@Mock
	private RestTemplate template;
	
	@Mock
	private HttpEntity<String> entity;
	
	private StarshipApi api;
	
	@Before
	public void setUp(){
		api = new StarshipApi(template,entity);
	}
	
	@SuppressWarnings("unchecked")
	@Test
	public void getStarshipModelSuccess() throws StarWarsException{
		StarshipModel expected = new StarshipModel();
		ResponseEntity<StarshipModel> mockResponse = mock(ResponseEntity.class);
		when(template.exchange(anyString(),any(HttpMethod.class), any(HttpEntity.class), any(ParameterizedTypeReference.class))).thenReturn(mockResponse);
		when(mockResponse.getBody()).thenReturn(expected);
		StarshipModel result = api.get(9);
		assertEquals(expected,result);
	}

	@SuppressWarnings("unchecked")
	@Test(expected=StarWarsException.class)
	public void getStarshipModelException() throws StarWarsException{
		StarshipModel expected = new StarshipModel();
		ResponseEntity<StarshipModel> mockResponse = mock(ResponseEntity.class);
		when(template.exchange(anyString(),any(HttpMethod.class), any(HttpEntity.class), any(ParameterizedTypeReference.class))).thenThrow(ResourceAccessException.class);
		when(mockResponse.getBody()).thenReturn(expected);
		StarshipModel result = api.get(9);
	}
	
	@SuppressWarnings("unchecked")
	@Test
	public void getAllStarshipModelSuccess() throws StarWarsException{
		List<StarshipModel> expected = new ArrayList<>();
		ResponseEntity<StarwarsListResponse<StarshipModel>> mockResponse = mock(ResponseEntity.class);
		StarwarsListResponse<StarshipModel> mockSWResponse = mock(StarwarsListResponse.class);
		when(template.exchange(anyString(),any(HttpMethod.class), any(HttpEntity.class), any(ParameterizedTypeReference.class))).thenReturn(mockResponse);
		when(mockResponse.getBody()).thenReturn(mockSWResponse);
		when(mockSWResponse.getResults()).thenReturn(expected);
		List<StarshipModel> result= api.getAll();
		assertEquals(expected,result);
	}
	
	@SuppressWarnings("unchecked")
	@Test(expected=StarWarsException.class)
	public void getAllStarshipModelsException() throws StarWarsException{
		List<StarshipModel> expected = new ArrayList<>();
		ResponseEntity<StarwarsListResponse<StarshipModel>> mockResponse = mock(ResponseEntity.class);
		StarwarsListResponse<StarshipModel> mockSWResponse = mock(StarwarsListResponse.class);
		when(template.exchange(anyString(),any(HttpMethod.class), any(HttpEntity.class), any(ParameterizedTypeReference.class))).thenThrow(ResourceAccessException.class);
		when(mockResponse.getBody()).thenReturn(mockSWResponse);
		when(mockSWResponse.getResults()).thenReturn(expected);
		List<StarshipModel> result= api.getAll();
	}
	
	@SuppressWarnings("unchecked")
	@Test
	public void getAllStarshipModelSuccessMultiPage() throws StarWarsException{
		List<StarshipModel> expected = new ArrayList<>();
		expected.add(new StarshipModel());
		ResponseEntity<StarwarsListResponse<StarshipModel>> mockResponse = mock(ResponseEntity.class);
		StarwarsListResponse<StarshipModel> mockSWResponse = mock(StarwarsListResponse.class);
		when(template.exchange(anyString(),any(HttpMethod.class), any(HttpEntity.class), any(ParameterizedTypeReference.class))).thenReturn(mockResponse);
		when(mockResponse.getBody()).thenReturn(mockSWResponse);
		when(mockSWResponse.getResults()).thenReturn(expected);
		when(mockSWResponse.getNext()).thenReturn("Next Page").thenReturn(null);
	
		List<StarshipModel> result= api.getAll();
		verify(template, atLeast(2)).exchange(anyString(),any(HttpMethod.class), any(HttpEntity.class), any(ParameterizedTypeReference.class));
		assertEquals(result.size(), 2);
	}
}
